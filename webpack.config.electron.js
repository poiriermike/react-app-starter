// tslint:disable
const path = require("path");

// Common config shared by both renderer and main configurations
module.exports = {
  target: "electron-main",

  node: {
    __dirname: false,
    __filename: false
  },

  module: {
    rules: [
      {
        include: [
          /src\/electron/
        ],
        exclude: [
          /src\/electron\/electron-wait-react.js/
        ],
      },
      // TS and TSX
      {
        loader: "ts-loader",
        test: /\.tsx?$/
      }
    ]
  },

  entry: [
    path.join(__dirname, "src", "electron", "electron.ts")  
  ],

  resolve: {
    extensions: [ ".tsx", ".ts", ".jsx", ".js", ".json" ]
  },

  output: {
    filename: "electron-bundle.js",
    path: path.resolve(__dirname, "build", "electron")
  }
};
