Visual Studio Code is to be used for projects.

1. Install NVM (Node Version Manager)
2. Install latest from NVM - 'nvm install latest'
3. Run command 'npm install' from the application root directory
4. Use the command 'npm run start:dev' and then in another terminal window 'npm run start:electron' to start the application in developer mode

Install whatever VS Code extensions you want, the following are recommended:
- npm
- TSLint
- GitLens — Git supercharged
- Material Theme
- Stylelint

**THINGS TO ADD/CHANGE/LOOK INTO**
- React Native
- React Snap(shot) (Next???) - Uploading the build directory to AWS S3 seemed to work just fine...
- Find a cross-platform logging solution
- Add Prettier to VSCode and/or npm packages

- Write tests (Jest for front end, and install Spectron for Electron)
