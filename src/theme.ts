import { createMuiTheme } from '@material-ui/core/styles';

/**
 * Define the gloal material UI theme
 */
export default createMuiTheme({
    palette: {
      type: 'dark'
    },
  });
