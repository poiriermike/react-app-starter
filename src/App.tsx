import { MuiThemeProvider } from '@material-ui/core/styles';
import './scss/css/main.css';
import * as React from 'react';
import { Provider } from 'react-redux';
import store from './store/store';
import theme from './theme';
import Todos from './containers/Todos';
// tslint:disable-next-line no-import-side-effect
import 'typeface-roboto';

/**
 * The main application class
 */
class App extends React.Component {
  render(): JSX.Element {
    return (
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <div className="app">
            <Todos />
          </div>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
