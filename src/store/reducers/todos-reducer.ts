import ITodo from '../../types/todo';
import { TodosAction, TodosActionCreators } from '../actions/todos-actions';
import { getType } from 'typesafe-actions';

/**
 * Todos container reducer declaration
 */

/**
 * Set the initial state
 */
const initialStore: ReadonlyArray<ITodo> = [];

/**
 * The Todos store reducer
 */
const todosReducer = (state: ReadonlyArray<ITodo> = initialStore, action: TodosAction): ReadonlyArray<ITodo> => {

  switch (action.type) {
    case getType(TodosActionCreators.addTodo):
      return state.concat(action.payload);
    case getType(TodosActionCreators.removeTodo):
      return state.filter((todo: ITodo) => todo.tid !== action.payload);
    default:
      return state;
  }
};

export default todosReducer;
