import { applyMiddleware, compose, createStore, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { RootActions } from './root-actions';
import { rootReducer, RootState } from './root-reducer';
import rootSaga from './root-saga';

/*
  This function is used to create and configure the redux store
*/
const configureStore = (): Store<RootState, RootActions> => {

  const sagaMiddleware = createSagaMiddleware();
  let store;

  // Use Redux Dev Tools is in development mode
  if (process.env.NODE_ENV === 'development') {
    // tslint:disable:no-unsafe-any
    const w: any = window as any;
    const devTools: any = w.devToolsExtension ? w.devToolsExtension() : (f: any): any => f;
    // tslint:enable:no-unsafe-any

    // Note that we could add preloaded state here, but we set the reducer functions to have default state
    store = createStore<RootState, RootActions, {}, {}>(rootReducer, compose(applyMiddleware(sagaMiddleware), devTools));
  } else {
    store = createStore<RootState, RootActions, {}, {}>(rootReducer, compose(applyMiddleware(sagaMiddleware)));
  }

  sagaMiddleware.run(rootSaga);
  return store;
};

export default configureStore();
