import ITodo from '../../types/todo';
import { ActionType, createAsyncAction, createStandardAction } from 'typesafe-actions';

/**
 * Todo action declarations
 */

enum ETodosAction {
  ADD = '[todos] ADD',
  REMOVE = '[todos] REMOVE',

  // The following are a part of the example async action
  ASYNC_REQUEST = '[todos] ASYNC_REQUEST',
  ASYNC_SUCCESS = '[todos] ASYNC_SUCCESS',
  ASYNC_FAILURE = '[todos] ASYNC_FAILURE',
}

export const TodosActionCreators = {
  addTodo: createStandardAction(ETodosAction.ADD)<ITodo>(),
  removeTodo: createStandardAction(ETodosAction.REMOVE)<string>(),

  asyncAction: createAsyncAction(
    ETodosAction.ASYNC_REQUEST, // Only the request is bound to a container
    ETodosAction.ASYNC_SUCCESS,
    ETodosAction.ASYNC_FAILURE
  )<void, ITodo, Error>()
};

export type TodosAction = ActionType<typeof TodosActionCreators>;
