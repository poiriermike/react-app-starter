import { delay } from 'redux-saga';
import { call, put, takeEvery } from 'redux-saga/effects';
import ITodo from '../../types/todo';
import { TodosAction, TodosActionCreators } from '../actions/todos-actions';
import todosApi from '../../api/todos-api';
import { getType } from 'typesafe-actions';

/**
 * Example saga
 */

 /**
  * This Worker saga makes the async call then either fires a success or failure action
  */
function* getAsyncSaga(action: TodosAction): Generator {
  try {
    yield delay(1000);
    const todo: ITodo = yield call(todosApi.getAsync);
    yield put(TodosActionCreators.asyncAction.success(todo));
  } catch (e) {
    yield put(TodosActionCreators.asyncAction.failure(e));
  }
}

/**
 * Our Watcher saga watches for actions dispatched to the store and startes our worker sagas
 */
function* asyncSaga(): Generator {
  yield takeEvery(getType(TodosActionCreators.asyncAction.request), getAsyncSaga); // Allows concurrent fetches of user.
  /*
    Alternatively you may use takeLatest.

    Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
    dispatched while a fetch is already pending, that pending fetch is cancelled
    and only the latest one will be run.
  */
  // yield takeLatest("USER_FETCH_REQUESTED", fetchUser);
}

export default asyncSaga;
