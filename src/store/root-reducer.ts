import { combineReducers } from 'redux';
import todosReducer from './reducers/todos-reducer';
import { StateType } from '../../node_modules/typesafe-actions';

/**
 * The root reducer is a combination of all containers reducers
 */
export const rootReducer = combineReducers({
  todos: todosReducer,
});

/**
 * The combined state of the app
 */
export type RootState = StateType<typeof rootReducer>;
