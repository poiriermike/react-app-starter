import { createSelector } from 'reselect';
import { RootState } from '../root-reducer';
import ITodo from '../../types/todo';

export const selectTodos = (state: RootState): ReadonlyArray<ITodo> => state.todos;

export const selectTodoCount = createSelector(
  [ selectTodos ],
  (todos: ReadonlyArray<ITodo>) => todos.length
);
