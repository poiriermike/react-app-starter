import { all, fork } from 'redux-saga/effects';
import asyncSaga from './sagas/todos-sagas';

/**
 * The root saga will execute all watcher sagas
 */
export default function* rootSaga(): Generator {
  yield all([
    fork(asyncSaga)
  ]);
}
