/**
 * Import all containers actions here and combine into the RootActions
 */
import { TodosAction } from './actions/todos-actions';

// Union of all actions
export type RootActions = TodosAction;
