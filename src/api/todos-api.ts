import axios, { AxiosPromise } from 'axios';
import ITodo from '../types/todo';

/**
 * This is just an example async request
 * Notice requests are prefixed with the type of the request
 */

 // This call will always fail
const getAsync = (): AxiosPromise<ITodo> => axios.get('/user?ID=12345');

export default {
  getAsync
};
