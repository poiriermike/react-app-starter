export default interface ITodo {
  readonly tid: string;

  readonly description?: string;
  readonly title?: string;
}
