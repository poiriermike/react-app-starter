import { AppBar, Button, createStyles, Grid, Theme, Toolbar, Typography, WithStyles, withStyles } from '@material-ui/core/';
import { StyleRules } from '@material-ui/core/styles';
import AddTodo from '../components/AddTodo';
import cn from 'classnames';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from '../store/root-reducer';
import Todo from '../components/Todo';
import ITodo from '../types/todo';
import { TodosActionCreators } from '../store/actions/todos-actions';
import * as todosSelectors from '../store/selectors/todos-selectors';

/**
 * Define material-ui styles
 */
const styles = (theme: Theme): StyleRules => createStyles({
  appBar: {
    height: 64
  },
  flexGrow: {
    flexGrow: 1
  }
});

/**
 * The props passed directly into this container
 * See mapStateToProps below to map the store state to props
 */
interface ITodosOwnProps { }

/**
 * Props passed in from the redux store
 * See mapStateToProps below to map the store state to props
 */
interface ITodosStateProps {
  readonly todos: ReadonlyArray<ITodo>;
  readonly todosCount: number;
}

/**
 * These are the actions this container is allowed to dispatch
 * mapDispatchToProps is actually mapping ALL actions (even disallowed ones),
 * but due to typesafety only actions defined here can be executed.
 * This allows us to not execute all actions from the container (some may be called after async actions complete)
 */
interface ITodosDispatchProps {
  readonly addTodo: typeof TodosActionCreators.addTodo;
  readonly removeTodo: typeof TodosActionCreators.removeTodo;

  // This is just an example async request
  readonly async: typeof TodosActionCreators.asyncAction.request;
}

/**
 * Combine the state and dispatch props together
 */
type TodosProps = ITodosOwnProps & ITodosStateProps & ITodosDispatchProps;

/**
 * This is state that is to be managed by the container. This should only be used with good reason.
 */
interface ITodosState {}

/**
 * Containers manage components and dispatch actions to redux (or pass dispatch functions down to the components)
 * Only containers should be hooked up to redux
 */
class Todos extends React.Component<TodosProps & WithStyles<typeof styles>, ITodosState> {
  render(): JSX.Element {
    const todos = this.props.todos.map((todo: ITodo | undefined, index: number | undefined) => {
      if (todo) {
        return (
          <Grid item key={todo.tid}>
            <Todo todo={todo} onDeleteClicked={this.props.removeTodo} />
          </Grid>
        );
      }

      return undefined;
    });

    return (
      <>
        <AppBar className={cn('app-bar', this.props.classes.appBar)}>
          <Toolbar>
            <Typography variant="h6" color="inherit" className={this.props.classes.flexGrow}>
              {this.props.todosCount}
            </Typography>
            <Button
              className="async-test-btn"
              variant="contained"
              color="secondary"
              onClick={this.props.async}
            >
              Async Test
            </Button>
          </Toolbar>
        </AppBar>
        <Grid container spacing={8}>
          {todos}
        </Grid>
        <Grid container spacing={8}>
          <Grid item>
            <AddTodo onAddClick={this.props.addTodo} />
          </Grid>
        </Grid>
      </>
    );
  }
}

/**
 * This is where we map the redux state to the containers props
 */
const mapStateToProps = (state: RootState, ownProps: ITodosOwnProps): ITodosStateProps => {
  const stateProps: ITodosStateProps = {
    todos: todosSelectors.selectTodos(state),
    todosCount: todosSelectors.selectTodoCount(state)
  };

  return stateProps;
};

/**
 * This is where all action creators are bound to the container. The props above specify which are allowed to be called.
 */
const mapDispatchToProps = (dispatch: Dispatch, ownProps: ITodosOwnProps): ITodosDispatchProps => bindActionCreators({
  addTodo: TodosActionCreators.addTodo,
  removeTodo: TodosActionCreators.removeTodo,

  // This is just an example async request
  async: TodosActionCreators.asyncAction.request
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(Todos));
