import App from './App';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
// import { debug } from 'electron-log'; // Using this is not cross-platform, only works for electron only apps
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './register-service-worker';

registerServiceWorker();
const rootEl: HTMLElement = document.getElementById('root') as HTMLElement;

/*
 * Setup axios interceptors to log all api requests
 */
axios.interceptors.request.use((request: AxiosRequestConfig): AxiosRequestConfig => {
  // debug(request);
  return request;
}, (err: any): any => {
  // debug(err);
  return Promise.reject(err);
});

/*
 * Setup axios interceptors to log all api responses
 */
axios.interceptors.response.use((response: AxiosResponse): AxiosResponse => {
  // debug(response);
  return response;
}, (err: any): any => {
  // debug(err);
  return Promise.reject(err);
});

if (process.env.NODE_ENV !== 'test') {
  // Render app
  ReactDOM.render(
    <App />,
    rootEl
  );

  // Enable hot module reload
  if (module.hot && process.env.NODE_ENV === 'development') {
    module.hot.accept();
  }
}
