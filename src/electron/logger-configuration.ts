import * as isDev from 'electron-is-dev';
import { debug, transports } from 'electron-log';

/**
 * This file is meant to setup the logger configuration
 */

/**
 * This function is called once on app startup to initialize the logger
 */
export const initLogging = (): void => {
  const maxBytes: number = 1e+6; // 1 Megabyte

  // Set the log level based on environment
  // tslint:disable:no-object-mutation
  transports.file.level = isDev ? 'silly' : 'info';
  transports.console.level = isDev ? 'silly' : 'info';
  transports.file.maxSize = maxBytes;
  // tslint:enable:no-object-mutation

  debug('Logging configuration set.');
};
