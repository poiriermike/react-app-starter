// tslint:disable-next-line no-implicit-dependencies - electron-builder does not allow electron as a dependency
import { app, BrowserWindow, BrowserWindowConstructorOptions } from 'electron';
import installExtension, { REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } from 'electron-devtools-installer';
import * as isDev from 'electron-is-dev';
import { debug, error } from 'electron-log';
import * as path from 'path';

/*
  Represents the main window of our application.
  Any required setup or event listeners should be dealt with here.
*/
export default class MainWindow extends BrowserWindow {

  constructor(URL: string, options?: BrowserWindowConstructorOptions) {
    super(options);

    this.once('ready-to-show', this.readyToShowHandler);
    this.on('closed', this.closedHandler);
    this.on('unresponsive', this.unresponsiveHandler);
    this.webContents.on('crashed', this.crashedHandler);

    this.loadURL(URL);

    if (isDev) {
      this.webContents.openDevTools();
      this.installExtensions();
    }
  }

  /*
    This is called in development mode to install various development tools
  */
  private readonly installExtensions = (): void => {
    if (!BrowserWindow.getDevToolsExtensions().hasOwnProperty('devtron')) {
      BrowserWindow.addDevToolsExtension(path.join(__dirname, '..', '..', 'node_modules', 'devtron'));
    }

    installExtension(REACT_DEVELOPER_TOOLS)
        .then((name: string) => debug(`Added Extension: ${name}`))
        .catch((err: string) => error(`An error occurred: ${err}`));

    installExtension(REDUX_DEVTOOLS)
        .then((name: string) => debug(`Added Extension: ${name}`))
        .catch((err: string) => error(`An error occurred: ${err}`));
  }

  /*
    Handles the ready-to-show event
  */
  private readonly readyToShowHandler = (): void => {
    debug('mainwindow:ready-to-show');
    this.show();
  }

  /*
    Handles the unresponsive event
  */
  private readonly unresponsiveHandler = (): void => {
    debug('mainwindow:unresponsive');
    app.quit();
  }

  /*
    Handles the closed event
  */
  private readonly closedHandler = (): void => {
    debug('mainwindow:closed');
    app.quit();
  }

  /*
    Handles the webContents.crashed event
  */
  private readonly crashedHandler = (): void => {
    debug('mainwindow:webContents:crashed');
    app.quit();
  }
}
