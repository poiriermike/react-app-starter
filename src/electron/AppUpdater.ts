import { debug, error, info, verbose } from 'electron-log';
import { autoUpdater } from 'electron-updater';

/**
 * This is an example using a GitLab. Edit this and the config to suit your needs.
 *
 * Wrapper for the electron-updater
 * Call checkForUpdates to initiate update check
 */
export default class AppUpdater {
  constructor() {
    autoUpdater.requestHeaders = { 'PRIVATE-TOKEN': 'Personal access Token' };
    autoUpdater.autoDownload = true;

    autoUpdater.setFeedURL({
      provider: 'generic',
      url: 'https://gitlab.com/_example_repo_/-/jobs/artifacts/master/raw/dist?job=build',
    });

    autoUpdater.on('checking-for-update', () => {
      info('Checking for update...');
    });

    autoUpdater.on('update-available', () => {
      info('Update available.');
    });

    autoUpdater.on('update-not-available', () => {
      info('Update not available.');
    });

    autoUpdater.on('error', () => {
      error('Error in auto-updater.');
    });

    autoUpdater.on('download-progress', (progressObj: {readonly [key: string]: string}) => {
      const logMessage: string = `Download speed: ${progressObj.bytesPerSecond}\n
                          Downloaded: ${parseInt(progressObj.percent, 10)}%\n
                          (${progressObj.transferred} / ${progressObj.total})\n`;
      verbose(logMessage);
    });

    autoUpdater.on('update-downloaded', () => {
      info('Update downloaded; will install in 1 seconds');
    });

    autoUpdater.on('update-downloaded', () => {
      const timeout: number = 1000;

      setTimeout (() => {
        autoUpdater.quitAndInstall();
      }, timeout);
    });
  }

  /**
   * This function is used to initialize an update check
   */
  readonly checkForUpdates = (): void => {
    autoUpdater.checkForUpdates()
    .then(() => debug('checkForUpdates succeeded'))
    .catch(debug);
  }
}
