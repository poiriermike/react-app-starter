// tslint:disable-next-line no-implicit-dependencies - electron-builder does not allow electron as a dependency
import { app, globalShortcut } from 'electron';
import * as isDev from 'electron-is-dev';
import { debug } from 'electron-log';
import { initLogging } from './logger-configuration';
import MainWindow from './MainWindow';
import * as path from 'path';
import * as url from 'url';

// tslint:disable-next-line:no-let
let mainWindow: MainWindow;
initLogging();

app.on('ready', () => {
  debug('app:ready');

  mainWindow = new MainWindow(
    process.env.ELECTRON_START_URL || url.format({
    pathname: path.join(__dirname, '..', 'index.html'),
    protocol: 'file:',
    slashes: true
  }), {
    frame: false,
    height: 720,
    resizable: true,
    show: false,
    width: 1280
  });

  /**
   * Close the app when escape is pressed
   */
  globalShortcut.register('Escape', () => {
    debug('app:Escape');
    app.quit();
  });
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  debug('app:window-all-closed');
  /**
   *  On OS X it is common for applications and their menu bar
   * to stay active until the user quits explicitly with Cmd + Q
   */
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  debug('app:activate');
  /**
   * On OS X it's common to re-create a window in the app when the
   * dock icon is clicked and there are no other windows open.
   */

  // Use this if necessary - test first
  if (!mainWindow) {
    // createWindow();
  }
});

process.on('uncaughtException', () => {
  debug('process:uncaughtException');
});

if (isDev) {
    app.on('certificate-error', (event: Electron.Event,
                                _1: Electron.WebContents,
                                _2: string,
                                _4: string,
                                _5: Electron.Certificate,
                                callback: (isTrusted: boolean) => void) => {
      debug('app:certificate-error');
      event.preventDefault();
      callback(true);
    }
  );
}
