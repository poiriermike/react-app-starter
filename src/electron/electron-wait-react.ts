import * as net from 'net';

/**
 * This helper module is used to allow electron and the dev server to start at the same time
 * and have electron wait for the server to be ready
 */

const port = process.env.PORT ? (+process.env.PORT - 100) : 3000;
// tslint:disable-next-line:no-object-mutation
process.env.ELECTRON_START_URL = process.env.HTTPS === 'true' ? `https://localhost:${port}` : `http://localhost:${port}`;
const client = new net.Socket();

const tryConnection = (): net.Socket => client.connect({port}, async() => {
        let startedElectron = false;

        client.end();
        if (!startedElectron) {
            startedElectron = true;
            const childProcess = await import('child_process');
            childProcess.exec('electron .');
        }
    }
);

tryConnection();

client.on('error', (error: any) => {
    setTimeout(tryConnection, 1000);
});
