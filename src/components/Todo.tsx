import { Button, Card, CardActions, CardContent, FormControl, Grid, Input, Typography } from '@material-ui/core/';
import * as React from 'react';
import ITodo from '../types/todo';

interface ITodoProps {
  readonly todo: ITodo;

  readonly onDeleteClicked: (tid: string) => void;
}

interface ITodoState {}

class Todo extends React.Component<ITodoProps, ITodoState> {
  render(): JSX.Element {
    return (
      <Card raised>
        <CardContent>
          <Grid direction="column" container spacing={8}>
            <Grid item>
              <Typography variant="h5" component="h3">
                {this.props.todo.title}
              </Typography>
            </Grid>
            <Grid item>
              <FormControl disabled>
                <Input disableUnderline multiline value={this.props.todo.description} />
              </FormControl>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button onClick={this.onDeleteClicked} variant="contained" color="secondary" size="small">Delete</Button>
        </CardActions>
      </Card>
    );
  }

  private readonly onDeleteClicked = (): void => {
    this.props.onDeleteClicked(this.props.todo.tid);
  }
}

export default Todo;
