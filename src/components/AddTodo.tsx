import { Button, Card, CardActions, CardContent, Grid, TextField } from '@material-ui/core/';
import * as cuid from 'cuid';
import * as React from 'react';
import ITodo from '../types/todo';

interface IAddTodoProps {
  readonly onAddClick: (todo: ITodo) => void;
}

interface IAddTodoState {
  readonly description: string;
  readonly error: boolean;
  readonly title: string;
}

class AddTodo extends React.Component<IAddTodoProps, IAddTodoState> {
  readonly state: IAddTodoState = {
    description: '',
    error: false,
    title: ''
  };

  render(): JSX.Element {
    return (
      <Card raised>
        <CardContent>
          <Grid direction="column" container spacing={8}>
            <Grid item>
            <TextField
              error={this.state.error}
              id="title"
              label="Title"
              value={this.state.title}
              onChange={this.handleTextChange('title')}
            />
            </Grid>
            <Grid item>
              <TextField
                id="description"
                multiline
                label="Description"
                value={this.state.description}
                onChange={this.handleTextChange('description')}
              />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button
            disabled={this.state.error}
            onClick={this.onAddClick}
            variant="contained"
            color="primary"
            size="small"
          >
              Add
          </Button>
        </CardActions>
      </Card>
    );
  }

  private readonly onAddClick = (): void => {
    if (!this.checkErrorState(this.state.title)) {
      this.props.onAddClick({
        tid: cuid(),

        description: this.state.description,
        title: this.state.title
      });

      this.setState({
        ...this.state,
        description: '',
        title: ''
      });
    } else {
      this.setState({
        ...this.state,
        error: true
      });
    }
  }

  private readonly handleTextChange = (id: string): (event: React.ChangeEvent<HTMLInputElement>) =>
    void => (event: React.ChangeEvent<HTMLInputElement>): void => {

    this.setState({
      ...this.state,
      [id]: event.target.value,
      error: this.checkErrorState(event.target.value)
    });

  }

  private readonly checkErrorState = (input: string): boolean => {
    if (input === '') {
      return true;
    }

    return false;
  }
}

export default AddTodo;
